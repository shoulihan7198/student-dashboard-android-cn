package com.seanhoulihan.studentdashboard;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;

public class MainActivity extends AppCompatActivity {

    private CoordinatorLayout coordinatorLayout;
    private SharedPreferences preferences;
    private CustomPagerAdapter customPagerAdapter;
    private ViewPager viewPager;
    private boolean isRestart;
    private Toolbar toolbar;
    private NestScraper nestScraper;
    private android.support.v4.app.FragmentManager fragmentManager;
    //private Fab actionButton;
    //private View sheetView;
    //private View overlay;
    //private MaterialSheetFab materialSheetFab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!isTaskRoot()) {
            finish();
            return;
        }
        setContentView(R.layout.activity_main);
        nestScraper = new NestScraper(getApplicationContext());
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator_layout);
        preferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);
        viewPager = (android.support.v4.view.ViewPager) findViewById(R.id.pager);
        fragmentManager = getSupportFragmentManager();

        if (savedInstanceState != null) {

            Calendar calendarFragment;
            Reports reportsFragment;
            isRestart = true;


            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                //Application was rotated from portrait to landscape
                calendarFragment = (Calendar) fragmentManager.findFragmentByTag("Calendar");
                reportsFragment = (Reports) fragmentManager.findFragmentByTag("Reports");
                final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.remove(calendarFragment);
                fragmentTransaction.remove(reportsFragment);
                Calendar newCalender = (Calendar) recreateFragment(calendarFragment);
                Reports newReports = (Reports) recreateFragment(reportsFragment);
                fragmentTransaction.add(R.id.calendar_frag_container, newCalender, "Calendar");
                fragmentTransaction.add(R.id.reports_frag_container, newReports, "Reports");
                fragmentTransaction.commit();
                fragmentManager.executePendingTransactions();


            } else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                //Application was rotated from landscape to portrait
                calendarFragment = (Calendar) fragmentManager.findFragmentByTag("Calendar");
                reportsFragment = (Reports) fragmentManager.findFragmentByTag("Reports");
                final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.remove(calendarFragment);
                fragmentTransaction.remove(reportsFragment);
                fragmentTransaction.commit();
                fragmentManager.executePendingTransactions();
                customPagerAdapter = new CustomPagerAdapter(getSupportFragmentManager(), viewPager, calendarFragment, reportsFragment);
                setUpPortrait();
            }

        } else {
            isRestart = false;


            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                //Application is starting up for the first time in landscape mode

                fragmentManager.beginTransaction().add(R.id.calendar_frag_container, new Calendar(), "Calendar").add(R.id.reports_frag_container, new Reports(), "Reports").commit();

            } else {
                //Application is starting up for the first time in portrait mode

                customPagerAdapter = new CustomPagerAdapter(getSupportFragmentManager(), viewPager);
                setUpPortrait();
            }
        }

        if (preferences.getBoolean("firstLaunch", true)) {
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
        } else {
            if (!isRestart) {
                nestScraper.logIn();
            }


        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        switch (id) {
            case R.id.action_log_out:
                final SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("loggedIn", false);
                editor.putBoolean("firstLaunch", true);
                editor.putString("username", "");
                editor.putString("password", "");
                editor.apply();
                nestScraper.LogOut();
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_view_schedule:

                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this)
                        .setView(R.layout.bell_schedule_dialog_layout)
                        .create();
                item.setIcon(getResources().getDrawable(R.drawable.ic_clear_24dp));
                alertDialog.show();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }


    private void setUpPortrait() {
        final TabLayout tabLayout = (TabLayout) findViewById(R.id.tablayout);
        if (Build.VERSION.SDK_INT < 20)
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.gold));
        viewPager.setAdapter(customPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        getSupportActionBar().setTitle(customPagerAdapter.getTitle(viewPager.getCurrentItem()));
        if (tabLayout.getTabCount() != 0) {
            for (int i = 0; i < tabLayout.getTabCount(); i++) {
                tabLayout.getTabAt(i).setIcon(customPagerAdapter.getDrawable(i));
            }
        }

        final Window window = getWindow();

        if (Build.VERSION.SDK_INT >= 22) {

            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {


                private int darkRed = getResources().getColor(R.color.redDark);
                private int darkGold = getResources().getColor(R.color.goldDark);
                private int red = getResources().getColor(R.color.redLight);
                private int gold = getResources().getColor(R.color.gold);
                ObjectAnimator animatorToolbar = ObjectAnimator.ofInt(toolbar, "backgroundColor", red, gold);
                ObjectAnimator animatorTab = ObjectAnimator.ofInt(tabLayout, "backgroundColor", red, gold);
                ValueAnimator valueAnimator = ValueAnimator.ofInt(darkRed, darkGold);


                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


                    animatorToolbar.setEvaluator(new ArgbEvaluator());
                    animatorTab.setEvaluator(new ArgbEvaluator());
                    valueAnimator.setEvaluator(new ArgbEvaluator());
                    if (positionOffset != 0f) {
                        animatorTab.setCurrentFraction(positionOffset);
                        animatorToolbar.setCurrentFraction(positionOffset);
                        valueAnimator.setCurrentFraction(positionOffset);
                        window.setStatusBarColor(((Integer) valueAnimator.getAnimatedValue()));
                    }

                }

                @Override
                public void onPageSelected(int position) {
                    getSupportActionBar().setTitle(customPagerAdapter.getTitle(position));


                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }


            });
        } else {

            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                private int darkRed = getResources().getColor(R.color.redDark);
                private int darkGold = getResources().getColor(R.color.goldDark);
                private int red = getResources().getColor(R.color.redLight);
                private int gold = getResources().getColor(R.color.gold);

                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    getSupportActionBar().setTitle(customPagerAdapter.getTitle(position));

                    if (Build.VERSION.SDK_INT == 21) {
                        if (position == 0) {
                            toolbar.setBackgroundColor(red);
                            tabLayout.setBackgroundColor(red);
                            window.setStatusBarColor(darkRed);
                        } else if (position == 1) {
                            toolbar.setBackgroundColor(gold);
                            tabLayout.setBackgroundColor(gold);
                            window.setStatusBarColor(darkGold);
                        }
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
        }

    }

    private Fragment recreateFragment(Fragment f) {
        try {
            Fragment.SavedState savedState = fragmentManager.saveFragmentInstanceState(f);

            Fragment newInstance = f.getClass().newInstance();
            newInstance.setInitialSavedState(savedState);

            return newInstance;
        } catch (Exception e) // InstantiationException, IllegalAccessException
        {
            throw new RuntimeException("Cannot reinstantiate fragment " + f.getClass().getName(), e);
        }
    }


}

