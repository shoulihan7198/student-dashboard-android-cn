package com.seanhoulihan.studentdashboard;

/**
 * Created by shoul on 9/25/2015.
 */
class GradebookEntry {

    private final String name;
    private final Double points_earned;
    private final Double points_available;
    private final String grade;
    private final String percent;
    private final boolean missing;
    private final String feedback;
    private final boolean late;

    public GradebookEntry(String name, Double points_earned, Double points_available, String percent, String grade, boolean missing, boolean late, String feedback) {
        this.name = name;
        this.points_earned = points_earned;
        this.points_available = points_available;
        this.grade = grade;
        this.percent = percent;
        this.missing = missing;
        this.late = late;
        this.feedback = feedback;
    }

    public String getPercent() {
        return percent;
    }

    public String getGrade() {

        return grade;
    }

    public Double getPoints_available() {

        return points_available;
    }

    public Double getPoints_earned() {

        return points_earned;
    }

    public String getName() {

        return name;
    }

    public boolean getMissing() {

        return missing;

    }

    public String getFeedback() {
        return feedback;
    }

    public boolean isLate() {
        return late;
    }


}
