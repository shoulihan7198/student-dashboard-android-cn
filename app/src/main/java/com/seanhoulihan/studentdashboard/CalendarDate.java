package com.seanhoulihan.studentdashboard;

import android.util.Log;

import org.joda.time.DateTime;

import java.util.ArrayList;

/**
 * Created by shoul on 9/29/2015.
 */
class CalendarDate {

    private final DateTime date;

    public ArrayList<Event> getEventArrayList() {
        return eventArrayList;
    }

    private ArrayList<Event> eventArrayList = new ArrayList<>();

    public String getWeekday() {
        return weekday;
    }

    public Integer getDay() {
        return day;
    }

    public String getMonth() {
        return month;
    }

    private Integer day;
    private String month;
    private String weekday;

    public CalendarDate(DateTime date, ArrayList<Event> eventArrayList) {
        this.date = date;
        this.eventArrayList = eventArrayList;
    }

    public CalendarDate(DateTime date) {
        this.date = date;
        day = date.getDayOfMonth();
        switch (date.getMonthOfYear()) {
            case 1:
                month = "January";
                break;
            case 2:
                month = "February";
                break;
            case 3:
                month = "March";
                break;
            case 4:
                month = "April";
                break;
            case 5:
                month = "May";
                break;
            case 6:
                month = "June";
                break;
            case 7:
                month = "July";
                break;
            case 8:
                month = "August";
                break;
            case 9:
                month = "September";
                break;
            case 10:
                month = "October";
                break;
            case 11:
                month = "November";
                break;
            case 12:
                month = "December";
                break;
        }

        String dayofweek = ((Integer) date.getDayOfWeek()).toString();

        switch (dayofweek) {
            case "7":
                weekday = "Sunday";
                break;
            case "1":
                weekday = "Monday";
                break;
            case "2":
                weekday = "Tuesday";
                break;
            case "3":
                weekday = "Wednesday";
                break;
            case "4":
                weekday = "Thursday";
                break;
            case "5":
                weekday = "Friday";
                break;
            case "6":
                weekday = "Saturday";
                break;

        }
        Log.d("CalendarDate", dayofweek);
    }

    public DateTime getDate() {
        return date;
    }

    public void addEvent(Event event) {
        eventArrayList.add(event);
    }
}
