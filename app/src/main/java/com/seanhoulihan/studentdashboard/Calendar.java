package com.seanhoulihan.studentdashboard;


import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.joda.time.DateTime;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class Calendar extends android.support.v4.app.Fragment implements DatesFetchedListener, LoginCompleteListener, SocketTimeoutListener, ErrorListener {

    private NestScraper nestScraper;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private SharedPreferences preferences;
    private CustomPagerAdapter pager;
    private List<CalendarDate> calendarDateList;
    private ActionBar toolbar;

    public Calendar() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.d("Calendar", "Calendar Fragment created!");
        View rootView = inflater.inflate(R.layout.fragment_calendar, container, false);
        toolbar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        setHasOptionsMenu(true);
        setRetainInstance(true);
        preferences = ((MainActivity) this.getActivity()).getSharedPreferences("preferences", Context.MODE_PRIVATE);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.calendar_recyclerview);
        recyclerView.setHasFixedSize(false);
        final LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.calendar_swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.redLight), getResources().getColor(R.color.gold));
        recyclerView.setLayoutManager(llm);
        nestScraper = new NestScraper(getContext());
        nestScraper.setOnLoginCompleteListener(this);
        nestScraper.setOnDatesFetchedListener(this);
        nestScraper.setOnSocketTimeoutListener(this);
        nestScraper.setOnErrorListener(this);
        if (!nestScraper.getCalendarDates().isEmpty()) {
            calendarDateList = nestScraper.getCalendarDates();
            recyclerView.setAdapter(new CustomRecyclerViewAdapter(calendarDateList, getActivity(), this));
        } else swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
            }
        });
        SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.d("Calendar", "onRefresh()");
                nestScraper.fetchEvents();
            }
        };
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);
        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                try {

                    if (pager.getViewPager().getCurrentItem() == 0)
                        toolbar.setTitle(llm.findViewByPosition(llm.findFirstVisibleItemPosition()).getTag().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        nestScraper.removeOnDatesFetchedListener(this);
        nestScraper.removeOnLoginCompleteListener(this);
        nestScraper.removeOnSocketTimeoutLIstener(this);
        nestScraper.removeOnErrorListener(this);
    }

    @Override
    public void onCreateOptionsMenu(
            Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_calendar, menu);

        if (!preferences.getBoolean("cancel", false)) {
            menu.findItem(R.id.action_calendar_view).setIcon(getResources().getDrawable(R.drawable.ic_remove_red_eye_24dp));
        } else {
            menu.findItem(R.id.action_calendar_view).setIcon(getResources().getDrawable(R.drawable.ic_clear_24dp));
        }

    }


    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_calendar_view) {

            final SharedPreferences.Editor editor = preferences.edit();
            boolean cancel = preferences.getBoolean("cancel", false);

            if (!cancel) {

                DateTime now = DateTime.now();
                com.borax12.materialdaterangepicker.date.DatePickerDialog datePicker = com.borax12.materialdaterangepicker.date.DatePickerDialog.newInstance(new com.borax12.materialdaterangepicker.date.DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(com.borax12.materialdaterangepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
                        DateTime startDate = new DateTime().withYear(year).withMonthOfYear(monthOfYear + 1).withDayOfMonth(dayOfMonth);
                        DateTime endDate = new DateTime().withYear(yearEnd).withMonthOfYear(monthOfYearEnd + 1).withDayOfMonth(dayOfMonthEnd);
                        if (!endDate.isAfter(startDate.toInstant())) {
                            Toast.makeText(getContext(), "Invalid date interval", Toast.LENGTH_LONG).show();
                        } else {
                            editor.putBoolean("cancel", true);
                            editor.apply();
                            item.setIcon(getResources().getDrawable(R.drawable.ic_clear_24dp));
                            nestScraper.setDateRange(startDate, endDate);
                            swipeRefreshLayout.setRefreshing(true);
                            nestScraper.fetchEvents();
                        }
                    }
                }, now.getYear(), now.getMonthOfYear() - 1, now.getDayOfMonth());
                datePicker.setYearRange(now.getYear() - 1, now.getYear() + 1);
                datePicker.setAccentColor(getContext().getResources().getColor(R.color.gold));
                datePicker.show(getActivity().getFragmentManager(), "calendar");

            } else {
                editor.putBoolean("cancel", false);
                editor.apply();
                item.setIcon(getResources().getDrawable(R.drawable.ic_remove_red_eye_24dp));
                swipeRefreshLayout.setRefreshing(true);
                nestScraper.setDateRange(null, null);
            }


        }

        return super.onOptionsItemSelected(item);
    }

    public void setPager(CustomPagerAdapter pager) {
        this.pager = pager;
    }


    @Override
    public void onDatesFetched() {
        calendarDateList = nestScraper.getCalendarDates();
        recyclerView.setAdapter(new CustomRecyclerViewAdapter(calendarDateList, getContext(), this));
        Log.d("Calendar", "onDatesFetched()");
        if (!nestScraper.getCalendarDates().isEmpty())
            swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onLoginComplete() {
        if (nestScraper.isSuccessfulLogin()) {
            Log.d("Calendar", "Login Complete");
            Snackbar.make(getActivity().findViewById(R.id.coordinator_layout), "Logged in", Snackbar.LENGTH_SHORT).show();
            nestScraper.fetchEvents();
            nestScraper.fetchGrades();
        } else {
            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity().getApplicationContext())
                    .setTitle("Network Error")
                    .setMessage("Check your connection. It is also possible The Nest is unresponsive or down at the moment. Please try again later.")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            builder.create().show();
        }


    }

    @Override
    public void onError() {
        Snackbar.make(getActivity().findViewById(R.id.coordinator_layout), "Some error occurred. www.cardinalnewman.org may be down.", Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onTimeout() {
        Snackbar.make(getActivity().findViewById(R.id.coordinator_layout), "Server timed out too many times, it may be under a lot of load.", Snackbar.LENGTH_LONG).show();
    }


}
