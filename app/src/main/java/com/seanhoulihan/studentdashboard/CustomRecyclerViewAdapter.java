package com.seanhoulihan.studentdashboard;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shoul on 10/4/2015.
 */
public class CustomRecyclerViewAdapter extends RecyclerView.Adapter<CustomRecyclerViewAdapter.CardViewHolder> {

    private final Context context;
    private final List<CalendarDate> calendarDateList = new ArrayList<>();
    private Calendar calendarFragment;


    public CustomRecyclerViewAdapter(List<CalendarDate> calendarDatesList, Context context, Calendar calendar) {
        for (int a = 0; a < calendarDatesList.size(); a++) {
            if (!calendarDatesList.get(a).getEventArrayList().isEmpty())
                calendarDateList.add(calendarDatesList.get(a));
        }


        this.context = context;
        this.calendarFragment = calendar;
        Log.d("dffd", ((Integer) calendarDateList.size()).toString());

    }

    @Override
    public int getItemCount() {
        return calendarDateList.size();
    }

    @Override
    public void onBindViewHolder(final CardViewHolder cardViewHolder, int i) {

        final CalendarDate cd = calendarDateList.get(i);
        cardViewHolder.eventLayout.removeAllViews();
        cardViewHolder.dayView.setText(cd.getDay().toString());
        cardViewHolder.weekdayView.setText(cd.getWeekday());
        for (int a = 0; a < cd.getEventArrayList().size(); a++) {
            final Event event = cd.getEventArrayList().get(a);
            final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View child;
            if (event.getClassName().isEmpty()) {

                if (event.hasDescription()) {
                    child = inflater.inflate(R.layout.event_row_layout_no_class_has_description, null);
                } else {
                    child = inflater.inflate(R.layout.event_row_layout_no_class, null);
                }

            } else {
                if (event.hasDescription()) {
                    child = inflater.inflate(R.layout.event_row_layout_has_description, null);
                } else {
                    child = inflater.inflate(R.layout.event_row_layout, null);
                }
                TextView eventClass = (TextView) child.findViewById(R.id.event_className);
                eventClass.setText(event.getClassName());

            }
            final TextView eventTitle = (TextView) child.findViewById(R.id.event_title);
            eventTitle.setText(event.getTitle());
            final LinearLayout layout = (LinearLayout) child.findViewById(R.id.child_layout);
            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(context, EventDetails.class);
                    intent.putExtra("dueDate", event.getDueDate());
                    intent.putExtra("eventTitle", event.getTitle());
                    intent.putExtra("eventClassName", event.getClassName());
                    intent.putExtra("eventDescription", event.getDescription());

                    context.startActivity(intent);
                    calendarFragment.getActivity().overridePendingTransition(R.anim.slide_up, R.anim.none);

                }
            });
            cardViewHolder.eventLayout.addView(child);
            cardViewHolder.itemView.setTag(cd.getMonth());
        }
    }


    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        View itemview = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.calendar_date_item, viewGroup, false);
        return new CardViewHolder(itemview);
    }


    public static class CardViewHolder extends RecyclerView.ViewHolder {

        final TextView dayView;
        final TextView weekdayView;
        final LinearLayout eventLayout;


        public CardViewHolder(View v) {
            super(v);
            dayView = (TextView) v.findViewById(R.id.dayView);
            weekdayView = (TextView) v.findViewById(R.id.weekdayView);
            eventLayout = (LinearLayout) v.findViewById(R.id.layout_for_events);
        }


    }
}
