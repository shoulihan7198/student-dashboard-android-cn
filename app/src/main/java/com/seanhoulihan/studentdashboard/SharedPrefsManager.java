package com.seanhoulihan.studentdashboard;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by shoul on 11/2/2015.
 */
public class SharedPrefsManager {

    private Context context;
    private SharedPreferences preferences;

    public SharedPrefsManager(Context context) {
        preferences = context.getSharedPreferences("preferences", Context.MODE_PRIVATE);
    }

    public SharedPreferences getPreferences() {
        return preferences;
    }

    public SharedPreferences.Editor getEditor() {
        return preferences.edit();
    }
}

