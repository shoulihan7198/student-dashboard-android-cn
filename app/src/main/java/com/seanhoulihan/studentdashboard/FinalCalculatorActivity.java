package com.seanhoulihan.studentdashboard;

import android.animation.Animator;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class FinalCalculatorActivity extends AppCompatActivity {

    private final static String TAG = "FINAL_CALC";
    private boolean revealed = false;
    private RelativeLayout contentView;
    private FrameLayout revealView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_calculator);
        final EditText percentWorthText = (EditText) findViewById(R.id.percent_worth_edit_text);
        final EditText percentWantedText = (EditText) findViewById(R.id.percent_wanted_edit_text);
        final Button calculateButton = (Button) findViewById(R.id.calculate_button);
        calculateButton.setEnabled(false);
        final double percent = Double.parseDouble(((Course) getIntent().getParcelableExtra("course")).getPercent().replace("%", "")) / 100.0;
        Log.d(TAG, String.valueOf(percent));
        percentWantedText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (percentWorthText.getText().toString().isEmpty() || percentWantedText.toString().isEmpty())
                    calculateButton.setEnabled(false);
                else calculateButton.setEnabled(true);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        percentWorthText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (percentWorthText.getText().toString().isEmpty() || percentWantedText.toString().isEmpty())
                    calculateButton.setEnabled(false);
                else calculateButton.setEnabled(true);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        calculateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double percent_worth = Double.parseDouble(percentWorthText.getText().toString().trim()) / 100.0;
                double percent_wanted = Double.parseDouble(percentWantedText.getText().toString().trim()) / 100.0;
                double grade_needed = Course.round((((percent_wanted - (percent * (1 - percent_worth))) / percent_worth) * 100), 2);
                Log.d(TAG, String.valueOf(percent_worth));
                Log.d(TAG, String.valueOf(percent_wanted));
                String pun;
                if (grade_needed > 90) {
                    pun = "you're going to have to study quite a bit";
                } else if (grade_needed > 80) {
                    pun = "you can do it!";
                } else if (grade_needed > 70) {
                    pun = "you probably won't mess it up";
                } else if (grade_needed > 60) {
                    pun = "it's going to be cake";
                } else {
                    pun = "you don't have anything to worry about";
                }

                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                contentView = (RelativeLayout) findViewById(R.id.content_view);
                revealView = (FrameLayout) findViewById(R.id.reveal_view);
                final TextView percentView = (TextView) findViewById(R.id.percent_needed_text);
                final TextView punView = (TextView) findViewById(R.id.pun);
                percentView.setText(String.valueOf(grade_needed) + "%");
                punView.setText(pun);
                int x = contentView.getMeasuredWidth() / 2;
                int y = contentView.getMeasuredHeight() / 2;
                int final_radius = Math.max(contentView.getWidth(), contentView.getHeight());
                Animator revealAnim = ViewAnimationUtils.createCircularReveal(revealView, x, y, 0, final_radius);
                revealView.setVisibility(View.VISIBLE);
                revealAnim.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        contentView.setVisibility(View.INVISIBLE);
                        revealed = true;
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        revealView.setVisibility(View.INVISIBLE);
                        contentView.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });
                revealAnim.start();

            }
        });

    }

    @Override
    public void onBackPressed() {
        if (revealed) {
            int x = revealView.getMeasuredWidth() / 2;
            int y = revealView.getMeasuredHeight() / 2;
            int initial_radius = revealView.getWidth() / 2;
            Animator utils = ViewAnimationUtils.createCircularReveal(revealView, x, y, initial_radius, 0f);
            contentView.setVisibility(View.VISIBLE);
            utils.start();
            utils.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    revealView.setVisibility(View.INVISIBLE);
                    revealed = false;
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
        } else {
            super.onBackPressed();
        }
    }
}
