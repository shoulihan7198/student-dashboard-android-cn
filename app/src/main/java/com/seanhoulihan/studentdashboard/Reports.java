package com.seanhoulihan.studentdashboard;


import android.animation.ObjectAnimator;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class Reports extends android.support.v4.app.Fragment implements GradesFetchedListener {


    private AnimatedExpandableListView expandableListView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private List<Course> courseList;
    private NestScraper nestScraper;
    private SwipeRefreshLayout.OnRefreshListener onRefreshListener;
    //private Fab actionButton;


    public Reports() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_reports, container, false);
        Log.d("Reports", "onCreateView()");
        setRetainInstance(true);
        expandableListView = (AnimatedExpandableListView) rootView.findViewById(R.id.reportListView);
        expandableListView.setGroupIndicator(null);
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.reports_swipe_refresh_layout);
        nestScraper = new NestScraper(getContext());
        nestScraper.setOnGradesFetchedListener(this);
        //actionButton = ((MainActivity) this.getActivity()).getFab();

        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.redLight), getResources().getColor(R.color.gold));

        if (!nestScraper.getCourseList().isEmpty()) {
            courseList = nestScraper.getCourseList();
            expandableListView.setAdapter(new CourseExpandableListAdapter(getActivity(), courseList));
        }
        else swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
            }
        });
        onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.d("Reports", "onRefresh()");
                nestScraper.fetchGrades();
            }
        };
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                ImageView indicator = (ImageView) v.findViewById(R.id.indicator);
                if (expandableListView.isGroupExpanded(groupPosition)) {
                    expandableListView.collapseGroupWithAnimation(groupPosition);
                    ObjectAnimator im = ObjectAnimator.ofFloat(indicator, "rotation", 180f, 360f);
                    im.setDuration(200);
                    im.start();
                } else {
                    expandableListView.expandGroupWithAnimation(groupPosition);
                    ObjectAnimator im = ObjectAnimator.ofFloat(indicator, "rotation", 0f, 180f);
                    im.setDuration(200);
                    im.start();

                }
                return true;
            }
        });


        return rootView;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        nestScraper.removeOnGradesFetchedListener(this);
    }

    @Override
    public void onGradesFetched() {
        courseList = nestScraper.getCourseList();
        expandableListView.setAdapter(new CourseExpandableListAdapter(getActivity(), courseList));
        swipeRefreshLayout.setRefreshing(false);
        Log.d("Reports", "onGradesFetched()");
    }


    public void setRefresh(boolean refreshing) {
        swipeRefreshLayout.setRefreshing(refreshing);
    }


}
