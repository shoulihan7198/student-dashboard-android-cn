package com.seanhoulihan.studentdashboard;

import org.joda.time.DateTime;

/**
 * Created by shoul on 9/29/2015.
 */
class Event {

    public String getClassName() {
        return className;
    }

    private final String className;
    private boolean hasDescription = true;

    public String getTitle() {
        return title;
    }

    private final String title;
    private final DateTime dueDate;

    public String getDescription() {
        return description;
    }

    private final String description;

    public Event(String className, String title, DateTime dueDate, String description) {
        this.className = className;
        if (title.startsWith(" ")) title = title.substring(1, title.length());
        this.title = title;
        this.dueDate = dueDate;
        this.description = description;
        if (description.isEmpty()) hasDescription = false;
    }

    public DateTime getDueDate() {
        return dueDate;
    }

    public boolean hasDescription() {
        return hasDescription;
    }
}
