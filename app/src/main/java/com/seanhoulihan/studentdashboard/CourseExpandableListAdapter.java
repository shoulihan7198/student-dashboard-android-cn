package com.seanhoulihan.studentdashboard;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by shoul on 9/25/2015.
 */
public class CourseExpandableListAdapter extends AnimatedExpandableListView.AnimatedExpandableListAdapter {

    private final List<Course> courseList;
    private final Activity context;

    public CourseExpandableListAdapter(Activity context, List<Course> courseList) {
        this.context = context;
        this.courseList = courseList;
    }

    @Override
    public int getGroupCount() {
        return courseList.size();
    }

    @Override
    public int getRealChildrenCount(int groupPosition) {
        return courseList.get(groupPosition).getGradebookEntries().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return courseList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return courseList.get(groupPosition).getGradebookEntries().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        final Course course = courseList.get(groupPosition);

        if (convertView == null) {
            LayoutInflater inflaInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflaInflater.inflate(R.layout.course_item, null);
        }
        final ImageView indicator = (ImageView) convertView.findViewById(R.id.indicator);
        if (courseList.get(groupPosition).hasMissing()) {
            indicator.setColorFilter(context.getResources().getColor(R.color.errorred));
        } else {
            indicator.setColorFilter(context.getResources().getColor(R.color.lightGray));
        }

        convertView.findViewById(R.id.course_options).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                PopupMenu popupMenu = new PopupMenu(v.getContext(), v);
                MenuInflater inflater = popupMenu.getMenuInflater();
                inflater.inflate(R.menu.menu_course, popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_course_options:
                                HashMap<String, String> categoryMap = course.getCategoryMap();
                                List<String> categoryList = new ArrayList<String>(categoryMap.keySet());
                                List<String> gradesList = new ArrayList<String>(categoryMap.values());
                                String text = "";
                                for (int i = 0; i < categoryList.size(); i++) {
                                    text = text + categoryList.get(i) + ": " + gradesList.get(i) + "%\n";
                                }
                                AlertDialog dialog = new AlertDialog.Builder(v.getContext())
                                        .setTitle("Course Information")
                                        .setMessage("Total points available: " + course.getPossiblePoints().toString() + "\nTotal points earned: " + course.getPointsEarned().toString() + "\n\n" + text)
                                        .setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        })
                                        .create();
                                dialog.show();
                                return true;
                            case R.id.action_calculate_final:

                                Intent intent = new Intent(v.getContext(), FinalCalculatorActivity.class);
                                intent.putExtra("course", course);
                                context.startActivity(intent);
                                return true;
                            default:
                                return false;
                        }
                    }


                });
                popupMenu.show();

            }
        });
        TextView CourseTitle = (TextView) convertView.findViewById(R.id.courseTitle);
        CourseTitle.setText(course.getName());
        TextView CoursePercent = (TextView) convertView.findViewById(R.id.percent);
        CoursePercent.setText(course.getPercent());
        TextView CourseGrade = (TextView) convertView.findViewById(R.id.grade);
        CourseGrade.setText(course.getLetter_grade());
        return convertView;
    }


    @Override
    public View getRealChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        GradebookEntry entry = courseList.get(groupPosition).getGradebookEntries().get(childPosition);
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.course_child_listview_item, parent, false);
            holder = new ViewHolder();
            convertView.setTag(holder);
        }
        boolean missing = entry.getMissing();
        boolean late = entry.isLate();
        holder = (ViewHolder) convertView.getTag();
        holder.title = (TextView) convertView.findViewById(R.id.assignment_title);
        holder.grade = (TextView) convertView.findViewById(R.id.assignment_grade);
        holder.percent = (TextView) convertView.findViewById(R.id.assignment_percent);
        holder.points = (TextView) convertView.findViewById(R.id.points);
        holder.feedback = (TextView) convertView.findViewById(R.id.assignment_feedback);
        String points;
        String percent;
        String grade;
        String feedback = entry.getFeedback();
        if (!feedback.trim().equals("")) {
            holder.feedback.setText(feedback);
            holder.feedback.setVisibility(View.VISIBLE);
        } else {
            holder.feedback.setVisibility(View.GONE);
        }
        if (!missing && !late && entry.getPoints_earned() == 0) {
            points = "-/-";
            grade = "-";
            percent = "-";
        } else {

            String earned_points = entry.getPoints_earned().toString();
            String total_points = entry.getPoints_available().toString();
            if (total_points.endsWith(".0"))
                total_points = total_points.replace(".0", "");
            if (earned_points.endsWith(".0"))
                earned_points = earned_points.replace(".0", "");
            points = earned_points + "/" + total_points;
            percent = entry.getPercent();
            grade = entry.getGrade();
        }
        holder.percent.setText(percent);
        holder.points.setText(points);
        holder.grade.setText(grade);
        holder.title.setText(entry.getName());
        holder.background = (LinearLayout) convertView.findViewById(R.id.child_layout);
        if (missing) {
            holder.background.setBackgroundColor(context.getResources().getColor(R.color.errorred));
            holder.title.setTextColor(context.getResources().getColor(R.color.white));
            holder.points.setTextColor(context.getResources().getColor(R.color.white));
            holder.percent.setTextColor(context.getResources().getColor(R.color.white));
            holder.grade.setTextColor(context.getResources().getColor(R.color.white));
        } else {
            holder.background.setBackgroundColor(context.getResources().getColor(R.color.lightGray));
            holder.title.setTextColor(context.getResources().getColor(R.color.black));
            holder.points.setTextColor(context.getResources().getColor(R.color.black));
            holder.percent.setTextColor(context.getResources().getColor(R.color.black));
            holder.grade.setTextColor(context.getResources().getColor(R.color.black));
        }
        return convertView;

    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    private static class ViewHolder {

        TextView title;
        TextView points;
        TextView grade;
        TextView percent;
        LinearLayout background;
        TextView feedback;

    }


}
