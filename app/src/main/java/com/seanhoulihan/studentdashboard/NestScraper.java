package com.seanhoulihan.studentdashboard;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

interface LoginCompleteListener {
    void onLoginComplete();
}

interface GradesFetchedListener {
    void onGradesFetched();
}

interface DatesFetchedListener {
    void onDatesFetched();
}

interface SocketTimeoutListener {
    void onTimeout();
}

interface ErrorListener {
    void onError();
}

public class NestScraper {

    private static final String LOGIN_URL = "https://www.cardinalnewman.org/userlogin.cfm?do=login&userrequest=true";
    private static final String DASHBOARD_URL = "https://www.cardinalnewman.org/groups.cfm";
    private static final String REPORT_URL = "https://www.cardinalnewman.org/cf_academics/ajax/lmsReportsDash.cfm";
    private static final String GRADEBOOK_ENTRIES_URL = "https://www.cardinalnewman.org/cf_academics/ajax/lmsGradebook.cfm";
    private static final String CALENDAR_PAGE_URL = "http://www.cardinalnewman.org/groups.cfm?groupDashboardView=calendar";
    private static final String CALENDAR_REQUEST_URL = "http://www.cardinalnewman.org/cf_calendar/ajax/fullcalendar.cfm";
    private static String password;
    private static String username;
    private static SimpleDate startDate;
    private static SimpleDate endDate;
    private static List<Course> courseList = new ArrayList<>();
    private static List<CalendarDate> calendarDates = new ArrayList<>();
    private static final List<DatesFetchedListener> datesFetchedListeners = new ArrayList<>();
    private static final List<LoginCompleteListener> listenersList = new ArrayList<>();
    private static final List<GradesFetchedListener> gradesFetchedListeners = new ArrayList<>();
    private static final List<SocketTimeoutListener> socketTimeoutListeners = new ArrayList<>();
    private static final List<ErrorListener> errorListeners = new ArrayList<>();
    private static Map<String, String> sessionCookies = new HashMap<>();
    private static String studentID;
    private static List<String> calIdsList = new ArrayList<>();
    private Context context;

    public NestScraper(Context context) {
        Log.d("NestScraper", "creating new NestScraper()");
        this.context = context;
        SharedPrefsManager manager = new SharedPrefsManager(context);
        SharedPreferences preferences = manager.getPreferences();
        if (!preferences.getBoolean("firstLaunch", true)) {
            password = preferences.getString("password", null);
            username = preferences.getString("username", null);
            String Start = preferences.getString("startSimple", null);
            String End = preferences.getString("endSimple", null);
            try {
                startDate = new Gson().fromJson(Start, SimpleDate.class);
                endDate = new Gson().fromJson(End, SimpleDate.class);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //setLoginCredentials to set username and password for first log in attempt
    public void setLoginCredentials(String u, String p) {
        password = p;
        username = u;
    }

    public void logIn() {

        AsyncTask<Void, Void, Void> logIn = new AsyncTask<Void, Void, Void>() {

            boolean socketTimeout = false;
            boolean error = false;

            @Override
            protected Void doInBackground(Void... voids) {
                SharedPreferences.Editor editor = new SharedPrefsManager(context).getEditor();

                for (int a = 0; a < 5; a++) {
                    try {
                        Connection.Response response = Jsoup
                                .connect(LOGIN_URL)
                                .data("username", username, "password", password)
                                .userAgent("Mozilla")
                                .timeout(10000)
                                .method(Connection.Method.POST)
                                .execute();
                        sessionCookies = response.cookies();
                        Document loginPage = Jsoup.connect(DASHBOARD_URL)
                                .cookies(sessionCookies)
                                .get();
                        ////Log.d("LoginActivity", "title: " + loginPage.title());
                        //Log.d("LoginActivity", "body: " + loginPage.body().toString());
                        if (loginPage.title().equals("Cardinal Newman High School:")) {
                            editor.putBoolean("loggedIn", true);
                            editor.putBoolean("firstLaunch", false);
                            editor.putString("username", username);
                            editor.putString("password", password);
                            editor.apply();
                            Elements studentidElements = loginPage.select(".mainGroup");
                            studentID = studentidElements.attr("data-personid");
                            //Log.d("getGrades", studentID);

                        } else {
                            editor.putBoolean("loggedIn", false);
                            editor.apply();
                        }

                    } catch (SocketTimeoutException t) {
                        //Log.d("LogIn", "retyring...");
                        if (a == 4) {
                            socketTimeout = true;
                        }
                        continue;

                    } catch (Exception e) {
                        e.printStackTrace();
                        editor.putBoolean("loggedIn", false);
                        editor.apply();
                        error = true;
                        break;
                    }
                    break;

                }
                fetchCalIds();
                return null;
            }

            protected void onPostExecute(Void dfd) {
                for (LoginCompleteListener l : listenersList) {
                    l.onLoginComplete();
                }
                if (socketTimeout) {
                    for (SocketTimeoutListener sl : socketTimeoutListeners) {
                        sl.onTimeout();
                    }
                }

                if (error) {
                    for (ErrorListener el : errorListeners) {
                        el.onError();
                    }
                }
            }

        };
        logIn.execute();

    }


    private List<Course> getClasses() {

        final List<Course> tempcourseList = new ArrayList<>();

        AsyncTask<Void, Void, Void> getGrades = new AsyncTask<Void, Void, Void>() {

            boolean socketTimeout = false;
            boolean error = false;

            @Override
            protected Void doInBackground(Void... params) {

                for (int x = 0; x < 5; x++) {

                    try {

                        JSONObject jsonObject = new JSONObject(Jsoup.connect(REPORT_URL)
                                .data("action", "getGradebookList", "studentid", studentID)
                                .method(Connection.Method.POST)
                                .cookies(sessionCookies)
                                .ignoreContentType(true)
                                .userAgent("Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.93 Safari/537.36")
                                .timeout(10000)
                                .header("Cache-Control", "max-age=0")
                                .header("Host", "www.cardinalnewman.org")
                                .header("Connection", "keep-alive")
                                .header("Content-Length", "38")
                                .header("Accept", "application/json, text/javascript, */*; q=0.01")
                                .header("Origin", "http://www.cardinalnewman.org")
                                .header("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
                                .header("X-Requested-With", "XMLHttpRequest")
                                .header("Referer", "http://www.cardinalnewman.org/groups.cfm?groupDashboardView=reports&ready")
                                .header("Accept-Encoding", "gzip, deflate")
                                .header("Accept-Language", "en-US,en;q=0.8")
                                .execute()
                                .body());

                        JSONArray array = jsonObject.getJSONArray("result");


                        for (int i = 0; i < array.length(); i++) {
                            try {
                                JSONObject tempObj = array.getJSONObject(i);
                                String grade = tempObj.get("YEARAVGNOTATEDGRADEBOOK").toString();
                                String percent = tempObj.get("YEARAVGGRADEBOOK").toString();
                                if (!percent.contains("N/A")) percent = percent + "%";
                                String course_title = tempObj.get("GROUPNAME").toString();
                                String gradebookID = tempObj.get("GRADEBOOKID").toString();
                                JSONArray array2 = tempObj.getJSONArray("MPDATA").getJSONObject(0).getJSONArray("CATEGORIES");
                                HashMap<String, String> categoryMap = new LinkedHashMap<>();
                                for (int a = 0; a < array2.length(); a++) {
                                    JSONObject object2 = array2.getJSONObject(a);
                                    String category = object2.get("CATEGORYNAME").toString();
                                    //Log.d("categories", category);
                                    String catgrade = object2.get("GRADE").toString();
                                    categoryMap.put(category, catgrade);
                                }
                                tempcourseList.add(new Course(course_title, percent, grade, gradebookID, categoryMap));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }


                    } catch (SocketTimeoutException t) {
                        if (x == 4) {
                            socketTimeout = true;
                        }
                    } catch (HttpStatusException h) {
                        //cookies are expired, need to re-login
                        logIn();
                        break;
                    } catch (Exception e) {
                        e.printStackTrace();
                        error = true;
                        break;
                    }
                    break;
                }
                return null;
            }

            protected void onPostExecute(Void temp) {
                if (socketTimeout) {
                    for (SocketTimeoutListener sl : socketTimeoutListeners) {
                        sl.onTimeout();
                    }
                }

                if (error) {
                    for (ErrorListener el : errorListeners) {
                        el.onError();
                    }
                }
            }
        };
        getGrades.execute();
        return tempcourseList;

    }

    private List<Course> getGradebookEntries(final List<Course> tempcourseList) {


        AsyncTask<Void, Void, Void> getGradebookEntries = new AsyncTask<Void, Void, Void>() {

            boolean socketTimeout = false;
            boolean error = false;


            @Override
            protected Void doInBackground(Void... params) {

                for (int x = 0; x < 5; x++) {

                    try {

                        for (int i = 0; i < tempcourseList.size(); i++) {
                            ArrayList<GradebookEntry> gradebookEntries = new ArrayList<>();
                            Course course = tempcourseList.get(i);
                            String gradebookID = course.getGradebookID();
                            JSONObject jsonObject = new JSONObject(Jsoup.connect(GRADEBOOK_ENTRIES_URL)
                                    .data("action", "getStudentEntries", "gradebookID", gradebookID, "personID", studentID, "markingperiodid", "4")
                                    .method(Connection.Method.POST)
                                    .cookies(sessionCookies)
                                    .ignoreContentType(true)
                                    .userAgent("Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.93 Safari/537.36")
                                    .timeout(10000)
                                    .header("Accept", "application/json, text/javascript, */*; q=0.01")
                                    .header("Accept-Encoding", "gzip, deflate")
                                    .header("Accept-Language", "en-US,en;q=0.8")
                                    .header("Connection", "keep-alive")
                                    .header("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
                                    .header("Host", "www.cardinalnewman.org")
                                    .header("Origin", "http://www.cardinalnewman.org")
                                    .header("Referer", "http://www.cardinalnewman.org/groups.cfm?groupDashboardView=reports&ready")
                                    .header("X-Requested-With", "XMLHttpRequest")
                                    .execute()
                                    .body());

                            JSONArray array = jsonObject.getJSONObject("result").getJSONArray("DATA");

                            for (int a = 0; a < array.length(); a++) {
                                JSONArray tempArray = array.getJSONArray(a);
                                boolean missing;
                                try {
                                    missing = tempArray.getBoolean(24);
                                } catch (Exception e) {
                                    missing = false;
                                }
                                boolean late;
                                try {
                                    late = tempArray.getBoolean(11);
                                } catch (Exception e) {
                                    late = false;
                                }


                                String name = tempArray.getString(13);
                                String feedback = tempArray.getString(12);
                                String total_points = ((Double) tempArray.getDouble(3)).toString();
                                String earned_points;
                                try {
                                    earned_points = ((Double) tempArray.getDouble(8)).toString();
                                } catch (JSONException e) {
                                    earned_points = "0";
                                }
                                Double percent = Double.parseDouble(earned_points) / Double.parseDouble(total_points);
                                String grade;
                                if (percent >= 0.895 && percent <= 1) {
                                    grade = "A";
                                } else if (percent >= 1) {
                                    grade = "A+";
                                } else if (percent >= 0.795 && percent < 0.895) {
                                    grade = "B";
                                } else if (percent >= 0.695 && percent < 0.795) {
                                    grade = "C";
                                } else if (percent >= 0.595 && percent < 0.695) {
                                    grade = "D";
                                } else if (percent < 0.595 && percent >= 0) {
                                    grade = "F";
                                } else {
                                    grade = "N/A";
                                }
                                percent = percent * 100;
                                String percent_str = (((Double) ((double) Math.round(percent * 100.0) / 100.0)).toString() + "%");
                                if (percent_str.endsWith(".0%"))
                                    percent_str = percent_str.replace(".0", "");

                                Double totalPoints = Double.parseDouble(total_points);
                                Double earnedPoints = Double.parseDouble(earned_points);

                                gradebookEntries.add(new GradebookEntry(name, earnedPoints, totalPoints, percent_str, grade, missing, late, feedback));
                                //Log.d("GetGrades", name + " " + earned_points + " " + total_points + " " + percent_str + " " + grade);
                            }
                            course.addGradebookEntries(gradebookEntries);
                        }

                    } catch (SocketTimeoutException t) {
                        if (x == 4) {
                            socketTimeout = true;
                        }
                        continue;
                    } catch (Exception e) {
                        e.printStackTrace();
                        error = true;
                        break;
                    }
                    break;
                }

                return null;
            }

            protected void onPostExecute(Void result) {
                if (!tempcourseList.isEmpty()) {
                    for (GradesFetchedListener fetchedListener : gradesFetchedListeners) {
                        fetchedListener.onGradesFetched();
                    }
                }

                if (socketTimeout) {
                    for (SocketTimeoutListener sl : socketTimeoutListeners) {
                        sl.onTimeout();
                    }
                }

                if (error) {
                    for (ErrorListener el : errorListeners) {
                        el.onError();
                    }
                }
            }
        };
        getGradebookEntries.execute();
        courseList = tempcourseList;
        return courseList;

    }


    private List<CalendarDate> getCalendarDates(final DateTime startDate, final DateTime endDate) {

        final String calId_str = TextUtils.join(",", calIdsList).replace("cal_", "");
        final List<Event> eventsList = new ArrayList<>();
        final String startString = convertDateToString(startDate);
        final String endString = convertDateToString(endDate);
        final int num_days = Days.daysBetween(startDate, endDate).getDays() + 1;
        //Log.d("getCalendarDates", startString);
        //Log.d("getCalendarDates", endString);
        //Log.d("getCalendarDates", calId_str);
        final List<CalendarDate> calendarDatesList = new ArrayList<>();
        for (int i = 0; i < num_days; i++) {
            CalendarDate calendardate = new CalendarDate(startDate.plusDays(i));
            calendarDatesList.add(calendardate);
            //Log.d("dfd", ((Integer) calendardate.getDate().getDayOfMonth()).toString());
        }

        final AsyncTask<Void, Void, Void> getCalendarDates = new AsyncTask<Void, Void, Void>() {

            boolean socketTimeout = false;
            boolean error = false;

            @Override
            protected Void doInBackground(Void... params) {

                for (int x = 0; x < 5; x++) {

                    try {

                        JSONObject jsonObject = new JSONObject(Jsoup.connect(CALENDAR_REQUEST_URL)
                                .data("action", "getEvents", "startDate", startString, "endDate", endString, "isMobile", "false", "isArchived", "false", "calendarIDList", calId_str, "teamIDList", "")
                                .method(Connection.Method.POST)
                                .cookies(sessionCookies)
                                .ignoreContentType(true)
                                .userAgent("Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.93 Safari/537.36")
                                .timeout(10000)
                                .header("Cache-Control", "max-age=0")
                                .header("Host", "www.cardinalnewman.org")
                                .header("Connection", "keep-alive")
                                .header("Accept", "application/json, text/javascript, */*; q=0.01")
                                .header("Origin", "http://www.cardinalnewman.org")
                                .header("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
                                .header("X-Requested-With", "XMLHttpRequest")
                                .header("Referer", "http://www.cardinalnewman.org/groups.cfm?groupDashboardView=calendar")
                                .header("Accept-Encoding", "gzip, deflate")
                                .header("Accept-Language", "en-US,en;q=0.8")
                                .execute()
                                .body());

                        JSONArray array = jsonObject.getJSONArray("result");
                        for (int i = 0; i < array.length(); i++) {

                            JSONObject object = array.getJSONObject(i);
                            String className = object.getString("calName");
                            String title = object.getString("title");
                            String description = object.getString("description");
                            DateTimeFormatter timeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd");
                            String dateString = object.getString("start").substring(0, 10);
                            //Log.d("calendar", dateString);
                            DateTime dueDate = DateTime.parse(dateString, timeFormatter);
                            Event event = new Event(className, title, dueDate, description);
                            //Log.d("getCalendarDates", title + " " + className + " " + description + " " + dueDate.toString());
                            eventsList.add(event);
                        }

                        for (int i = 0; i < calendarDatesList.size(); i++) {
                            CalendarDate calendarDate = calendarDatesList.get(i);
                            for (int a = 0; a < eventsList.size(); a++) {
                                Event event = eventsList.get(a);
                                if (calendarDate.getDate().getDayOfMonth() == event.getDueDate().getDayOfMonth() && calendarDate.getDate().getMonthOfYear() == event.getDueDate().getMonthOfYear()) {
                                    calendarDate.addEvent(event);
                                    //Log.d("dfdf", "event added to date");
                                    //Log.d("dfdf", event.getTitle());
                                }
                            }
                        }
                    } catch (SocketTimeoutException st) {
                        if (x == 4) {
                            socketTimeout = true;
                        }
                        continue;

                    } catch (HttpStatusException h) {
                        //cookies are expired, need to re-login
                        logIn();
                        break;
                    } catch (Exception e) {
                        e.printStackTrace();
                        error = true;
                        break;
                    }
                    break;
                }

                return null;
            }

            protected void onPostExecute(Void temp) {

                if (socketTimeout) {
                    for (SocketTimeoutListener sl : socketTimeoutListeners) {
                        sl.onTimeout();
                    }
                } else if (error) {
                    for (ErrorListener el : errorListeners) {
                        el.onError();
                    }
                } else {
                    for (DatesFetchedListener d : datesFetchedListeners) {
                        d.onDatesFetched();
                        Log.d("NestScraper", "Notifying Listeners");
                    }
                }


            }
        };
        getCalendarDates.execute();
        return calendarDatesList;

    }


    public boolean isSuccessfulLogin() {
        return new SharedPrefsManager(context).getPreferences().getBoolean("loggedIn", false);
    }

    public List<Course> getCourseList() {
        return courseList;

    }


    public void fetchGrades() {

        courseList = getGradebookEntries(getClasses());
    }

    private void fetchCalIds() {


        for (int a = 0; a < 5; a++) {


            try {

                Document calendarPage = Jsoup.connect(CALENDAR_PAGE_URL)
                        .cookies(sessionCookies)
                        .get();

                Elements calIds = calendarPage.select("option[value^=cal_]");
                for (int i = 0; i < calIds.size(); i++) {
                    String calID = calIds.get(i).attr("value");
                    calIdsList.add(calID);
                    //Log.d("getCalIds", calID);
                }

            } catch (SocketTimeoutException t) {
                Log.d("calendar", "retrying...");
                continue;

            } catch (Exception e) {
                e.printStackTrace();
                break;
            }
            break;

        }

    }


    public void fetchEvents() {
        if (startDate != null && endDate != null) {
            DateTime startD = new DateTime().withYear(startDate.year).withMonthOfYear(startDate.month).withDayOfMonth(startDate.day);
            DateTime endD = new DateTime().withYear(endDate.year).withMonthOfYear(endDate.month).withDayOfMonth(endDate.day);
            calendarDates = getCalendarDates(startD, endD);
        } else calendarDates = getCalendarDates(DateTime.now(), DateTime.now().plusDays(14));
    }


    public List<CalendarDate> getCalendarDates() {
        return calendarDates;
    }

    private String convertDateToString(DateTime date) {
        String dayOfMonth;
        Integer day_of_month = date.getDayOfMonth();
        if (day_of_month.toString().length() == 1) {
            dayOfMonth = "0" + day_of_month;
        } else {
            dayOfMonth = day_of_month.toString();
        }
        String monthOfYear;
        Integer month_of_year = date.getMonthOfYear();
        if (month_of_year.toString().length() == 1) {
            monthOfYear = "0" + month_of_year;
        } else {
            monthOfYear = month_of_year.toString();
        }

        return monthOfYear + "/" + dayOfMonth + "/" + ((Integer) date.getYear()).toString();

    }

    public void setOnLoginCompleteListener(LoginCompleteListener listenerToAdd) {
        listenersList.add(listenerToAdd);
    }

    public void removeOnLoginCompleteListener(LoginCompleteListener listenerToRemove) {
        listenersList.remove(listenerToRemove);
    }

    public void setOnGradesFetchedListener(GradesFetchedListener listenerToAdd) {
        gradesFetchedListeners.add(listenerToAdd);
    }

    public void removeOnGradesFetchedListener(GradesFetchedListener listenerToRemove) {
        gradesFetchedListeners.remove(listenerToRemove);
    }

    public void setOnDatesFetchedListener(DatesFetchedListener listenerToAdd) {
        datesFetchedListeners.add(listenerToAdd);
    }

    public void removeOnDatesFetchedListener(DatesFetchedListener listenerToRemove) {
        datesFetchedListeners.remove(listenerToRemove);
    }

    public void setOnSocketTimeoutListener(SocketTimeoutListener listenerToAdd) {
        socketTimeoutListeners.add(listenerToAdd);
    }

    public void removeOnSocketTimeoutLIstener(SocketTimeoutListener listenerToRemove) {
        socketTimeoutListeners.remove(listenerToRemove);
    }

    public void setOnErrorListener(ErrorListener listenerToAdd) {
        errorListeners.add(listenerToAdd);
    }

    public void removeOnErrorListener(ErrorListener listenerToRemove) {
        errorListeners.remove(listenerToRemove);
    }


    public void LogOut() {
        if (!courseList.isEmpty()) courseList.clear();
        if (!calIdsList.isEmpty()) calIdsList.clear();
        if (!calendarDates.isEmpty()) calendarDates.clear();
        if (!studentID.isEmpty()) studentID = null;
        if (!sessionCookies.isEmpty()) sessionCookies.clear();

        for (DatesFetchedListener d : datesFetchedListeners) {
            d.onDatesFetched();
        }

        for (GradesFetchedListener fetchedListener : gradesFetchedListeners) {
            fetchedListener.onGradesFetched();
        }


        //updates the data displayed to being blank

    }

    public void setDateRange(DateTime startDate, DateTime endDate) {
        SharedPreferences.Editor editor = new SharedPrefsManager(context).getEditor();
        if (startDate != null && endDate != null) {
            SimpleDate startSimple = new SimpleDate();
            startSimple.year = startDate.getYear();
            startSimple.month = startDate.getMonthOfYear();
            startSimple.day = startDate.getDayOfMonth();
            NestScraper.startDate = startSimple;
            SimpleDate endSimple = new SimpleDate();
            endSimple.year = endDate.getYear();
            endSimple.month = endDate.getMonthOfYear();
            endSimple.day = endDate.getDayOfMonth();
            NestScraper.endDate = endSimple;
            editor.putString("startSimple", new Gson().toJson(startSimple));
            editor.putString("endSimple", new Gson().toJson(endSimple));
            editor.apply();
        } else {
            editor.putString("startSimple", null);
            editor.putString("endSimple", null);
            NestScraper.startDate = null;
            NestScraper.endDate = null;

            editor.apply();
        }
        fetchEvents();
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public static String getStudentID() {
        return studentID;
    }

    public static SimpleDate getStartDate() {
        return startDate;
    }

    public static SimpleDate getEndDate() {
        return endDate;
    }

    public static List<DatesFetchedListener> getDatesFetchedListeners() {
        return datesFetchedListeners;
    }

    public static List<LoginCompleteListener> getListenersList() {
        return listenersList;
    }

    public static List<GradesFetchedListener> getGradesFetchedListeners() {
        return gradesFetchedListeners;
    }

    public static List<SocketTimeoutListener> getSocketTimeoutListeners() {
        return socketTimeoutListeners;
    }

    public static List<ErrorListener> getErrorListeners() {
        return errorListeners;
    }

    public static Map<String, String> getSessionCookies() {
        return sessionCookies;
    }

    public static List<String> getCalIdsList() {
        return calIdsList;
    }

    public void finalize() {
        try {
            super.finalize();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        Log.d("NestScraper", "destroying NestScraper");
    }
}
