package com.seanhoulihan.studentdashboard;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;


public class LoginActivity extends AppCompatActivity {

    private ErrorListener errorListener;
    private SocketTimeoutListener socketTimeoutListener;
    private NestScraper nestScraper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setFinishOnTouchOutside(false);
        final CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.dialog_coordinator_layout);
        nestScraper = new NestScraper(getApplicationContext());
        final Button loginBtn = (Button) findViewById(R.id.login_btn);
        final CircularProgressView circularBar = (CircularProgressView) findViewById(R.id.progress_circle);
        final TextInputLayout usernameField = (TextInputLayout) findViewById(R.id.username);
        final TextInputLayout passwordField = (TextInputLayout) findViewById(R.id.password);
        passwordField.setTypeface(Typeface.SANS_SERIF);
        usernameField.requestFocus();
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                passwordField.setError(null);
                usernameField.setError(null);
                String password = passwordField.getEditText().getText().toString();
                String username = usernameField.getEditText().getText().toString();
                final LoginCompleteListener loginCompleteListener = new LoginCompleteListener() {
                    @Override
                    public void onLoginComplete() {
                        if (nestScraper.isSuccessfulLogin()) {
                            killActivity();
                        } else {
                            loginBtn.setVisibility(View.VISIBLE);
                            circularBar.setVisibility(View.GONE);
                            usernameField.requestFocus();
                            Snackbar.make(coordinatorLayout, "  Invalid credentials  ", Snackbar.LENGTH_SHORT).show();

                        }
                        nestScraper.removeOnLoginCompleteListener(this);
                    }
                };
                if (!username.isEmpty() && !password.isEmpty()) {

                    nestScraper.setOnLoginCompleteListener(loginCompleteListener);
                    loginBtn.setVisibility(View.INVISIBLE);
                    circularBar.setVisibility(View.VISIBLE);
                    circularBar.startAnimation();
                    nestScraper.setLoginCredentials(username, password);
                    nestScraper.logIn();

                } else {
                    if (username.isEmpty()) usernameField.setError("Invalid username");
                    if (password.isEmpty()) passwordField.setError("Invalid password");
                }
            }


        });
        errorListener = new ErrorListener() {
            @Override
            public void onError() {
                Log.d("Login", "onError");
                Toast.makeText(getApplicationContext(), "www.cardinalnewman.org may be down.", Toast.LENGTH_LONG).show();
            }
        };

        nestScraper.setOnErrorListener(errorListener);
        socketTimeoutListener = new SocketTimeoutListener() {
            @Override
            public void onTimeout() {
                Snackbar.make(coordinatorLayout, "Server timed out too many times, it may be under a lot of load.", Snackbar.LENGTH_LONG).show();
            }
        };


    }

    @Override
    public void onBackPressed() {

    }

    private void killActivity() {
        nestScraper.removeOnErrorListener(errorListener);
        nestScraper.removeOnSocketTimeoutLIstener(socketTimeoutListener);
        finish();
    }

}
