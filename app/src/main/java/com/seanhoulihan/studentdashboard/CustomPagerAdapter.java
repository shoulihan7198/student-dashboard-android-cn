package com.seanhoulihan.studentdashboard;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by shoul on 9/20/2015.
 */
class CustomPagerAdapter extends PagerAdapter {

    private final int numPages = 2;
    private ViewPager pager;
    private FragmentManager fragmentManager;
    private Fragment mCurrentPrimaryItem = null;
    private Calendar calendarFragment = null;
    private Reports reportsFragment = null;
    private FragmentTransaction mCurTransaction = null;

    @Override
    public int getCount() {
        return numPages;
    }

    public CustomPagerAdapter(FragmentManager fragmentManager, ViewPager pager) {
        super();
        this.fragmentManager = fragmentManager;
        this.pager = pager;
    }

    public CustomPagerAdapter(FragmentManager fragmentManager, ViewPager pager, Calendar calendar, Reports reports) {
        super();
        this.fragmentManager = fragmentManager;
        this.pager = pager;
        this.calendarFragment = calendar;
        this.reportsFragment = reports;

    }

    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                if (calendarFragment == null) {
                    Calendar calendar = new Calendar();
                    calendar.setPager(this);
                    return calendar;
                } else {
                    calendarFragment.setPager(this);
                    return calendarFragment;
                }

            case 1:
                if (reportsFragment == null) {
                    return new Reports();
                } else {
                    return reportsFragment;
                }
            default:
                return null;
        }
    }

    public int getDrawable(int position) {
        switch (position) {
            case 0:
                return R.drawable.ic_calendar;
            case 1:
                return R.drawable.ic_reports;
            default:
                return 0;
        }
    }

    public CharSequence getTitle(int position) {
        switch (position) {
            case 0:
                return "Calendar";
            case 1:
                return "Reports";
            default:
                return "Student Dashboard";
        }
    }

    public ViewPager getViewPager() {
        return pager;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        if (mCurTransaction == null) {
            mCurTransaction = fragmentManager.beginTransaction();
        }

        String name = (position == 0) ? "Calendar" : "Reports";
        Fragment fragment = fragmentManager.findFragmentByTag(name);
        if (fragment != null) {
            mCurTransaction.attach(fragment);
        } else {
            fragment = getItem(position);
            mCurTransaction.add(container.getId(), fragment, name);
        }

        if (fragment != mCurrentPrimaryItem) {
            fragment.setMenuVisibility(false);
            fragment.setUserVisibleHint(false);
        }

        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        if (mCurTransaction == null) {
            mCurTransaction = fragmentManager.beginTransaction();
        }
        mCurTransaction.detach((Fragment) object);
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        Fragment fragment = (Fragment) object;
        if (fragment != mCurrentPrimaryItem) {
            if (mCurrentPrimaryItem != null) {
                mCurrentPrimaryItem.setMenuVisibility(false);
                mCurrentPrimaryItem.setUserVisibleHint(false);
            }
            if (fragment != null) {
                fragment.setMenuVisibility(true);
                fragment.setUserVisibleHint(true);
            }
            mCurrentPrimaryItem = fragment;
        }
    }

    @Override
    public void finishUpdate(ViewGroup container) {
        if (mCurTransaction != null) {
            mCurTransaction.commitAllowingStateLoss();
            mCurTransaction = null;
            fragmentManager.executePendingTransactions();
        }
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return ((Fragment) object).getView() == view;
    }
}



