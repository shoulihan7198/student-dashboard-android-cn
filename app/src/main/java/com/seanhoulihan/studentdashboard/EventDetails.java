package com.seanhoulihan.studentdashboard;

import android.content.Intent;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.ViewManager;
import android.widget.TextView;

import org.joda.time.DateTime;

import java.util.GregorianCalendar;

public class EventDetails extends AppCompatActivity {

    private static final int CIRCULAR_REVEAL_ANIMATION_LENGTH = 600;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_details_layout);
        final String eventTitleString = getIntent().getStringExtra("eventTitle");
        final String eventClassNameString = getIntent().getStringExtra("eventClassName");
        final String eventDescriptionString = getIntent().getStringExtra("eventDescription");
        final DateTime date = (DateTime) getIntent().getSerializableExtra("dueDate");

        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        TextView eventDescription = (TextView) findViewById(R.id.event_description);
        TextView eventTitle = (TextView) findViewById(R.id.event_title);
        TextView eventClassName = (TextView) findViewById(R.id.event_class_name);

        eventTitle.setText(eventTitleString);


        eventDescription.setText(Html.fromHtml(eventDescriptionString).toString());
        eventDescription.setMovementMethod(LinkMovementMethod.getInstance());


        if (eventClassNameString.isEmpty()) {
            ((ViewManager) eventClassName.getParent()).removeView(eventClassName);
        } else {
            eventClassName.setText(eventClassNameString);
        }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent calendarIntent = new Intent(Intent.ACTION_EDIT);
                calendarIntent.setType("vnd.android.cursor.item/event");
                calendarIntent.putExtra(CalendarContract.Events.TITLE, eventTitleString);
                if (!eventDescriptionString.isEmpty())
                    calendarIntent.putExtra(CalendarContract.Events.DESCRIPTION, eventDescriptionString);
                GregorianCalendar cal = new GregorianCalendar(date.getYear(), (date.getMonthOfYear() - 1), date.getDayOfMonth());
                Log.d("Calendar", ((Integer) date.getDayOfMonth()).toString() + " " + date.getMonthOfYear());
                Log.d("Calendar", ((Long) cal.getTimeInMillis()).toString());
                calendarIntent.putExtra("beginTime", cal.getTimeInMillis());
                calendarIntent.putExtra("allDay", true);
                startActivity(calendarIntent);
            }
        });



    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.none, R.anim.slide_down);
    }

}
