package com.seanhoulihan.studentdashboard;

import android.os.Parcel;
import android.os.Parcelable;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by shoul on 9/25/2015.
 */
class Course implements Parcelable {

    private String name;
    private String percent;
    private Double possible_points;
    private Double gotten_points;
    private String letter_grade;
    private boolean has_missing = false;
    private String gradebookID;
    private HashMap<String, String> categoryMap = new HashMap<>();
    private List<GradebookEntry> gradebookEntries = new ArrayList<>();

    public Course(String name, String percent, String letter_grade, String gradebookID, HashMap<String, String> categoryMap) {
        this.name = name;
        this.letter_grade = letter_grade;
        this.percent = percent;
        this.gradebookID = gradebookID;
        this.categoryMap = categoryMap;
    }

    public void addGradebookEntries(ArrayList<GradebookEntry> gradebookEntries) {
        this.gradebookEntries = gradebookEntries;
        double earned_points = 0d;
        double total_points = 0d;
        for (int i = 0; i < gradebookEntries.size(); i++) {
            GradebookEntry entry = gradebookEntries.get(i);
            if (entry.getMissing()) has_missing = true;
            if (!entry.getMissing() && !entry.isLate() && entry.getPoints_earned() == 0) continue;
            earned_points += entry.getPoints_earned();
            total_points += entry.getPoints_available();
        }
        possible_points = total_points;
        gotten_points = round(earned_points, 2);

    }

    public List<GradebookEntry> getGradebookEntries() {
        return gradebookEntries;
    }

    public String getLetter_grade() {
        return letter_grade;
    }

    public String getPercent() {

        return percent;
    }

    public String getName() {

        return name;
    }

    public boolean hasMissing() {
        return has_missing;
    }


    public Double getPossiblePoints() {
        return possible_points;
    }

    public Double getPointsEarned() {
        return gotten_points;
    }

    public String getGradebookID() {
        return gradebookID;
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public HashMap<String, String> getCategoryMap() {
        return categoryMap;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.percent);
    }

    protected Course(Parcel in) {
        this.percent = in.readString();
    }

    public static final Parcelable.Creator<Course> CREATOR = new Parcelable.Creator<Course>() {
        public Course createFromParcel(Parcel source) {
            return new Course(source);
        }

        public Course[] newArray(int size) {
            return new Course[size];
        }
    };
}
